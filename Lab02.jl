### A Pluto.jl notebook ###
# v0.14.2

using Markdown
using InteractiveUtils

# ╔═╡ 45bf7e01-8dd4-4abd-ae46-d608b731ee9d
md"# State definitions "

# ╔═╡ da11a750-a1d8-11eb-195a-dfc44f9a6052
struct State
	name::String
	position::Int64
	dirt::Vector{Bool}
end

# ╔═╡ c63693f6-7df0-487e-b1bb-1227c373ab4c
md"# Action Definition "

# ╔═╡ 19830334-aefb-4cb7-a618-d2d5eea0c8b8
struct Action
	name::String
	cost::Int64
end

# ╔═╡ e6b83960-4f83-4c92-bff0-0a1d791442f9
md"### Assigning actions to squares"

# ╔═╡ 70243b7d-7433-4f14-adf0-066957d4e84a
A1 = Action("Left", 1)

# ╔═╡ aef3c411-aeb3-4e17-88aa-e96b742c0d57
A2 = Action("Right", 2)

# ╔═╡ 273b8f95-73f8-495d-9903-c7128265a581
A3 = Action("Pick_Up", 3)

# ╔═╡ 2fc3f013-740a-47fd-91fd-c67d2fbf96fd
md"### Assigning the states to the squares"

# ╔═╡ bfe89c9d-0ae7-4de1-ba90-d3541ef7de68
S1 = State("Square_A", 1, [true, true, true])

# ╔═╡ 54232250-d658-4723-acf6-de448d48d9f1
S2 = State("Square_B", 2, [true, true, true])

# ╔═╡ c02bcb30-8bf6-4d09-af28-c3f7f50c8aa1
S3 = State("Square_C", 3, [true, true, true])

# ╔═╡ 7e4b95da-99a6-4251-b43c-b98292478b00
S4 = State("Square_A", 1, [false, true, true])

# ╔═╡ 909e1b23-6953-4957-bcdb-84dfb993f998
S5 = State("Square_B", 5, [false, true, true])

# ╔═╡ e3afb090-3ca9-4771-9398-d2bbf1384a79
S6 = State("Square_C", 3, [false, true, false])

# ╔═╡ 638f3ac7-c642-48dc-94a0-2fc26a4ee633
md"# Create transition model"

# ╔═╡ 7c7d903a-0591-4e92-9fcd-adb38db6a089
Transition_Mod = Dict()

# ╔═╡ 2ccea3b1-eac0-426a-ab64-4f340de01d75
md"### Add a mapping/push!  to the transition Model"

# ╔═╡ 9fc91dde-70a1-4d40-aa63-d8f8fd719b59
push!(Transition_Mod, S1 => [(A2, S2), (A1, S1), (A3, S3)])

# ╔═╡ d2d4257d-48ad-4ab3-997a-66bd6e466779
push!(Transition_Mod, S2 => [(A2, S3), (A1, S1)])

# ╔═╡ 8290be9a-60cf-40b5-814b-86019ef78337
push!(Transition_Mod, S3 => [(A2, S2), (A1, S2)])

# ╔═╡ c6c84321-8e0e-4ed8-afef-d3867fdd8a25
push!(Transition_Mod, S4 => [(A1, S4), (A2, S5)])

# ╔═╡ 025a91e1-9b01-4289-bd59-ea11dde47285
push!(Transition_Mod, S5 => [(A1, S4), (A2, S6)])

# ╔═╡ 21646d90-7d03-4f4d-9495-0077c4c797e0
push!(Transition_Mod, S6 => [(A1, S5), (A2, S6), (A3, S6)])

# ╔═╡ 4f62ce72-2aa7-4feb-9141-71ce26da9398
md"# Write the search strategy and then traverse the ntire state space"

# ╔═╡ 3aa61e8b-fa23-49c7-a7c8-17e3f2d2e9b5
function Search_Strategy(Transition_Model, Goal_State)#function arguments are only useful inside the function
	Transition_Model::String
	Goal_State::Vector{Int64}
	Initial::{Int64}
	
	result = []
	frontier = Queue{State}()
	explored = []
	enqueue!(frontier, Initial_State)
	first_state = false
	while true
		if is empty(frontier)
			return []
		else
			current_state = dequeue!(frontier)
			if first_state
				first_state = false
			else past_candidate = Transition_Model[explored[length(explored)]]#if state im visiting is not
				if single_candidate[2] == current_state#if past candidate is equal to currnt state then push into result
					push!(result, single_candidate, past_candidate[2])
			push!(explored, current_state)#exploring candidate startt from here
			candidates = Transition_Model[(current_state)]#[(),(),()]
			for single_candidate in candidates
				if!(single_candidate[2] in Goal_State)#if not explored yet check goal test
					return result#if
					else enqueue!(frontier, single_candidate[2])#if not put vector in candidate
						end
					end
				end
			end
		end
	end
end
	

# ╔═╡ 50fbe98f-ad83-4bea-a2b4-5d3f24e07617
begin
result = []
#frontier = Queue{State}()
	Enqueue!(frontier, Initial_State)
end

# ╔═╡ Cell order:
# ╟─45bf7e01-8dd4-4abd-ae46-d608b731ee9d
# ╠═da11a750-a1d8-11eb-195a-dfc44f9a6052
# ╟─c63693f6-7df0-487e-b1bb-1227c373ab4c
# ╠═19830334-aefb-4cb7-a618-d2d5eea0c8b8
# ╟─e6b83960-4f83-4c92-bff0-0a1d791442f9
# ╠═70243b7d-7433-4f14-adf0-066957d4e84a
# ╠═aef3c411-aeb3-4e17-88aa-e96b742c0d57
# ╠═273b8f95-73f8-495d-9903-c7128265a581
# ╟─2fc3f013-740a-47fd-91fd-c67d2fbf96fd
# ╠═bfe89c9d-0ae7-4de1-ba90-d3541ef7de68
# ╠═54232250-d658-4723-acf6-de448d48d9f1
# ╠═c02bcb30-8bf6-4d09-af28-c3f7f50c8aa1
# ╠═7e4b95da-99a6-4251-b43c-b98292478b00
# ╠═909e1b23-6953-4957-bcdb-84dfb993f998
# ╠═e3afb090-3ca9-4771-9398-d2bbf1384a79
# ╠═638f3ac7-c642-48dc-94a0-2fc26a4ee633
# ╠═7c7d903a-0591-4e92-9fcd-adb38db6a089
# ╠═2ccea3b1-eac0-426a-ab64-4f340de01d75
# ╠═9fc91dde-70a1-4d40-aa63-d8f8fd719b59
# ╠═d2d4257d-48ad-4ab3-997a-66bd6e466779
# ╠═8290be9a-60cf-40b5-814b-86019ef78337
# ╠═c6c84321-8e0e-4ed8-afef-d3867fdd8a25
# ╠═025a91e1-9b01-4289-bd59-ea11dde47285
# ╠═21646d90-7d03-4f4d-9495-0077c4c797e0
# ╟─4f62ce72-2aa7-4feb-9141-71ce26da9398
# ╠═3aa61e8b-fa23-49c7-a7c8-17e3f2d2e9b5
# ╠═50fbe98f-ad83-4bea-a2b4-5d3f24e07617
